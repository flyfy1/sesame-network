# Setup

Note that you'll need to build on top of the existing code I've provided

- In the Manifest file, add the followings:
		
		<manifest>
		... ...
		<uses-permission android:name="android.permission.INTERNET"/>
	    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE"/>
    	<uses-permission android:name="android.permission.ACCESS_WIFI_STATE"/>
    	<uses-permission android:name="android.permission.CHANGE_WIFI_MULTICAST_STATE"/>
    	<uses-permission android:name="android.permission.CHANGE_WIFI_STATE" />
    	<uses-permission android:name="android.permission.CHANGE_NETWORK_STATE" />
    	
    	<application>
    	... ...
    		<activity android:name=".network.activity.PeerSettingActivity"/>
    	</application>
    	... ...
		<manifest>

- In the activity which you want to use WiFi direct, you'll need to extend the `sg.edu.nus.sesame.enhancement.NetworkActivityTemplate` class, instead of activity class, and override the following method in the following way:
	
			protected boolean performConnectionDiscovery(){return true;}

- If you want to let the user select deveice to make connection, then you can start `sg.edu.nus.sesame.network.activity.PeerSettingActivity`. This activity would allow user to select devices to connect to

# Use

## Data Package Object
`sg.edu.nus.sesame.network.NetworkService` is the most useful class here. It provides an universal API for sending/receiving data; `sg.edu.nus.sesame.network.impl.NetworkMessageObject` is the `data package`. It has the following fields: 

	- long sendTime;
	- byte code;		 // Designed to be used by the user freely
	- byte internalCode; // Not exposed to the API user
	- byte[] sourceIP;
	- byte[] targetIP;
	- byte[] messageInBytes; // All data are sent out in bytes, 
							 // and the API user needs to interpret the data themselves.

The fields are pretty self-explainary.

## Handle Received Message

You can register different `MessageHandleListener` to your NetworkService. For example, the following code would register a "echo server":

	registerMessageHandler(new MessageHandleListener() {
		@Override
		public boolean handleMessage(NetworkMessageObject message) {
			if(message.code == ECHO_SERVER){
				byte[] tempIP = message.sourceIP;
				message.sourceIP = message.targetIP;
				message.targetIP = tempIP;
				return true;
			} else{
				return false;
			}
		}
	});

`return true` means that this message has been handled, so that other messageHandleListener don't need to do unnecessary checking.

## Send Information

If one wants to send out information, simply call:

	networkService.sendMessage(messageObject);

## Gain instance of NetworkService

At any point of time, calling `WiFiDirectBroadcastConnectionController.getNetworkService()` would give you an instance of NetworkService. It can be a client, or a server; but it doesn't appear to have any difference from the user's view.

## Getting Connected Devices

At any point of time, calling `ConnectedDevices.getConnectedIps()` to get an array of IP address of the connected nodes. The first one is the group-owner.

You can also register an `observer` to the ConnectedDevices, so that you can be notified when there's any change to the current list; the current list would be changed every time when there's there's a new device joining the network.

# Known Issues
If the multiple devices are connected, you'll need to start program in the group owner first. If you start the other devices first, then it would not be able to connect to the server becase the server is hosted at the group owner side, and it's not yet started.