package sg.edu.nus.sesame.main.view;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

public class ConnectedIPListAdapter extends BaseAdapter implements ListAdapter {
	String ips[] = new String[0];
	Context mContext;
	
	public ConnectedIPListAdapter(Context applicationContext) {
		mContext = applicationContext;
	}

	public int getCount() {
		return ips.length;
	}

	public Object getItem(int position) {
		return ips[position];
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView == null ? View.inflate(mContext,
				android.R.layout.simple_list_item_1, null) : convertView;
		view.setPadding(10, 10, 10, 10);
		String ip = (String) getItem(position);
		TextView tvIP = (TextView) view.findViewById(android.R.id.text1);
		tvIP.setText(ip);
		return view;
	}
}
