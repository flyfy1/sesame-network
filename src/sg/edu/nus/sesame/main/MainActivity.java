package sg.edu.nus.sesame.main;

import java.util.Arrays;
import java.util.Observable;
import java.util.Observer;

import sg.edu.nus.sesame.R;
import sg.edu.nus.sesame.enhancement.ApplicationHelper;
import sg.edu.nus.sesame.enhancement.NetworkActivityTemplate;
import sg.edu.nus.sesame.main.view.ConnectedIPListAdapter;
import sg.edu.nus.sesame.network.Utils;
import sg.edu.nus.sesame.network.activity.PeerSettingActivity;
import sg.edu.nus.sesame.network.controller.WiFiDirectBroadcastConnectionController;
import sg.edu.nus.sesame.network.impl.InternalMessage;
import sg.edu.nus.sesame.network.impl.NetworkMessageObject;
import sg.edu.nus.sesame.network.model.ConnectedDevices;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;

public class MainActivity extends NetworkActivityTemplate implements OnClickListener{
	ConnectedIPListAdapter phoneListAdapter;
	Activity self = this;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		final LinearLayout layout = (LinearLayout) findViewById(R.id.phoneListBtns);
		
		final LayoutParams btnParam = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		
		ConnectedDevices.addObserver(new Observer() {
			@Override
			public void update(Observable observable, Object data) {
				final String[] theIps = (String[]) data;
				System.out.println("Update of the observer called, data = " + Arrays.toString(theIps));
				
				ApplicationHelper.getActivityInstance().runOnUiThread(new Runnable() {
					@Override public void run() {
						layout.removeAllViews();
						for(String ip: theIps){
							Button btn = new Button(self);
							btn.setLayoutParams(btnParam);
							btn.setText(ip);
							layout.addView(btn);
							btn.setOnClickListener(new OnClickListener() {
								@Override
								public void onClick(View v) {
									Button b = (Button) v;
									String ip = b.getText().toString();
									byte targetIp[] = Utils.getBytesFromIp(ip);
									System.out.println("Target IP got: " + Utils.getIpAddressAsString(targetIp));
									NetworkMessageObject msg = new InternalMessage("Hello~~",(byte)4,Utils.getIPv4AddressOfInterface("p2p"),targetIp);
									WiFiDirectBroadcastConnectionController.getNetworkService().sendMessage(msg);
								}
							});
						}
					}
				});
			}
		});
		
		for(int viewId: viewIdsToListenTo) findViewById(viewId).setOnClickListener(this);
	}
	
	// TODO: "Start Activity for result" may better illustrate what I wanna do here.
	
	protected boolean performConnectionDiscovery(){return true;}
	private int viewIdsToListenTo[] = {R.id.btn_find_peer, R.id.btn_refresh};
	@Override
	public void onClick(View v) {
		Intent intent;
		
		switch (v.getId()) {
		case R.id.btn_find_peer:
			intent = new Intent(this, PeerSettingActivity.class);
			startActivity(intent);
			break;
		case R.id.btn_refresh:
//			phoneListAdapter.update(null, Phone.getCurrentList());
			Log.v("DEBUG","need to refresh");
		default:
			break;
		}
	}
}
