package sg.edu.nus.sesame.enhancement;

public interface PauseResumeListener {
	public void onPause();
	public void onResume();
}
