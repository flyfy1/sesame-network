package sg.edu.nus.sesame.network.activity;

import sg.edu.nus.sesame.R;
import sg.edu.nus.sesame.enhancement.NetworkActivityTemplate;
import sg.edu.nus.sesame.network.controller.WiFiDirectBroadcastConnectionController;
import sg.edu.nus.sesame.network.model.Phone;
import sg.edu.nus.sesame.network.view.WifiListAdapter;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.net.wifi.p2p.WifiP2pDevice;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class PeerSettingActivity extends NetworkActivityTemplate implements OnItemClickListener{
	private static final int ACTION_DISCONNECT = 1;
	private static final int ACTION_CONNECT = 2;
	private ListView lv;
	WifiListAdapter adapter;
	
	@Override
	protected boolean performConnectionDiscovery(){return true;}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_peer_setting);
		
		lv = (ListView) findViewById(R.id.lvPhonesList);
		adapter = new WifiListAdapter(this); 
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(this);
		WiFiDirectBroadcastConnectionController.getInstance().discoverPeers();
	}
	
	public void clickToRefreshPeerList(View v){
		WiFiDirectBroadcastConnectionController.getInstance().discoverPeers();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		final Phone p = (Phone) adapter.getItem(position);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		final int action;
        
		if(p.deviceInfo.status == WifiP2pDevice.CONNECTED){
			action = ACTION_DISCONNECT;
		} else{
			action = ACTION_CONNECT;
		}
		
		builder.setMessage(getActionStr(action, true) + " phone?")
		       .setPositiveButton(getActionStr(action,false), new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int id) {
		                switch(action){
		                case ACTION_CONNECT:
		                	Log.v("DEBUG", "Going to connect the phone");
		                	networkController.connectToPhone(p);
		                	break;
		                case ACTION_DISCONNECT:
		                	Log.v("DEBUG", "Going to disconnect the phone");
		                	break;
		                }
		            }
		        })
		        .setNegativeButton("Concel", new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int id) {}
		        });
		 builder.show();
	}
	
	private String getActionStr(int actionType, boolean isMessage){
		switch(actionType){
		case ACTION_CONNECT: return "Connect" + ((isMessage) ? " to" : "");
		case ACTION_DISCONNECT: return "Disconnect" + ((isMessage) ? " from" : "");
		default: return "OK";
		}
	}
}
