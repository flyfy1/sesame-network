package sg.edu.nus.sesame.network.controller;

import sg.edu.nus.sesame.enhancement.ApplicationHelper;
import sg.edu.nus.sesame.network.NetworkService.MessageHandleListener;
import sg.edu.nus.sesame.network.impl.NetworkMessageObject;
import sg.edu.nus.sesame.network.model.Phone;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.ActionListener;
import android.net.wifi.p2p.WifiP2pManager.PeerListListener;
import android.util.Log;

public class WiFiDirectBroadcastConnectionHelper {
	static final PeerListListener peerListListener = new PeerListListener() {
		@Override
		public void onPeersAvailable(WifiP2pDeviceList peers) {
			Phone.initList(peers.getDeviceList());
		}
	};
	
	public static final WifiP2pManager.ActionListener peerActionListener = new WifiP2pManager.ActionListener(){
		@Override
		public void onSuccess() {
			ApplicationHelper.showToastMessage("Peer Discovery Succeeded. Called from Controller");
			Log.i("DEBUG", "Peer Discovery Succeeded. Called from Controller");
		}

		@Override
		public void onFailure(int reason) {
			Log.i("DEBUG", "Peer Discovery Failed. Called from Controller");
		}
	};
	
	static final ActionListener peerDiscoveryActionalListener = new ActionListener() {
		@Override
		public void onSuccess() {
			Log.i("DEBUG", "Connection Successful.");
		}
		
		@Override
		public void onFailure(int reason) {
			Log.i("DEBUG", "Connection Failed. Reason Code: " +reason);
		}
	};

	protected static final MessageHandleListener messageHandler = new MessageHandleListener() {
		@Override
		public boolean handleMessage(NetworkMessageObject messageObj) {
			// TODO: undone yet... net to handle message here.
			/*
			String message = messageObj.getMessageInBytes();
			Log.i("DEBUG",messageObj.getSourceIP() + " : " + messageObj.getMessgage());
			if(messageObj.getMessageCode() == MessageCode.startMessureDist){
				Log.i("DEBUG","Going to do action: " + messageObj.getMessageCode());
				MainActivity.getInstance().doAction(MainActivityActionCode.StartMeassureDist,null);
			} else{
				Log.i("DEBUG","Not going to do action: " + messageObj.getMessageCode());
			}
			 */
			return false;
		}
	};
}
