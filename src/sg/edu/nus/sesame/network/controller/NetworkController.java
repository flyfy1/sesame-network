package sg.edu.nus.sesame.network.controller;

import sg.edu.nus.sesame.enhancement.PauseResumeListener;
import sg.edu.nus.sesame.network.model.Phone;
import android.app.Activity;

public class NetworkController implements PauseResumeListener {
	private Activity act;
	public boolean performConnectionDiscovery = true;
	
	private WiFiDirectBroadcastConnectionController mConnection;
	
	public NetworkController(Activity act){
		this.act = act;
		mConnection = WiFiDirectBroadcastConnectionController.getInstance();
	}
	
	@Override
	public void onResume(){
		if(performConnectionDiscovery)	act.registerReceiver(mConnection, mConnection.getIntentFilter());
	}
	
	@Override
	public void onPause(){
		if(performConnectionDiscovery)	act.unregisterReceiver(mConnection);
	}

	public void connectToPhone(final Phone p) {
    	mConnection.connectToDevice(p.deviceInfo,  WiFiDirectBroadcastConnectionHelper.peerActionListener);
	}
}
